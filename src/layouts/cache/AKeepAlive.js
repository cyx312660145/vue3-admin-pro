import { KeepAlive, h, toRaw } from "vue"
import {components } from './components'
import { useRoute } from "vue-router"

export default {
  props:{
    include: [String, RegExp, Array],
    exclude: [String, RegExp, Array],
    max: [String, Number],
    clearCaches: Array
  },
  setup(props, context) {
    const {include, exclude, max} = props
    
    function pruneCacheEntry(){
      const clearCaches = toRaw(props.clearCaches)
      if(clearCaches && clearCaches.length > 0){
        clearCaches.forEach(key => {
          const {value: component, key: componentKey} = components.get(key)
          components.set(key , {
            value: component, 
            key: componentKey + '/' +new Date().valueOf()
          })
          context.emit('update:clearCaches', [])
        })
      }
    }
    
    function createComponent (){
      pruneCacheEntry()
      const route = useRoute()
      const componentPath = route.fullPath
      if(components.has(componentPath)){
        const {value: component, key: componentKey} = components.get(componentPath)
        const component_config = {
          key: componentKey
        }
        return h(component, component_config)
      }
    }

    return {
      createComponent,
      include, 
      exclude, 
      max,
    }
  },
  render() {
    const keep_alive_config = {
      include: this.include,
      exclude: this.exclude,
      max:this.max
    }
    return h(KeepAlive, keep_alive_config, {
      default: () => this.createComponent()
    })
  },
}