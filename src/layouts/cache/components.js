import { useRouter } from 'vue-router'

const components = new Map()

function getType(data) {
  return Object.prototype.toString.call(data)
}

function setComponents(data) {
  data.forEach(item => {
    components.set(item.key, {
      value: item.value,
      key: item.key
    })
  });
}

function createComponents() {
  const router = useRouter()
  let routes = router.options.routes
  routes = routes.filter(e => e.path == process.env.VUE_APP_HOME_PATH)
  return new Promise((resolve) => {
    const data = []
    const func = async (routes) => {
      for(let item of routes){
        const component = item.component ? item.component : null
        if(getType(component) === '[object Function]'){
          const {default: cpt} = await component()
          data.push({
            value: cpt,
            key: item.fullPath,
          })
        }
        if(item.children && item.children.length > 0){
          await func(item.children)
        }
      }
    }
    func(routes).then(()=>{
      setComponents(data)
      resolve(components)
    })
  });
}


export {components, createComponents}