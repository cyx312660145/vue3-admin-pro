import { createApp } from 'vue'
import Antd from 'ant-design-vue';
import App from './App.vue'
import router from './router'
import store from './store'
import plugins from '@/plugins'
import initUtil from '@/utils'
import 'ant-design-vue/dist/reset.css';
import 'animate.css/source/animate.css'


createApp(App).use(plugins).use(store).use(router).use(Antd).mount('#app')
initUtil(router, store)
