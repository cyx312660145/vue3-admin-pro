import aIcon from '@/components/a-icon/a-icon'

// 注册 公共组件
export default {
  install(Vue){
    Vue.component('a-icon', aIcon)
  }
}
