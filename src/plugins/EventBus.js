import mitt from "mitt"

export default {
  install(Vue) { 
    const EventBus = mitt()
    Vue.provide('$EventBus', EventBus)
    window.onresize = () => {
      EventBus.emit('widthChange', document.body.clientWidth)
    }
  }
}