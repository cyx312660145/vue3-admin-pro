import GComponents from './CommonComponents'
import EventBus from './EventBus'
import fix from './fix'

export default {
  install(Vue) {
    Vue.use(fix)
    Vue.use(GComponents)
    Vue.use(EventBus)
  }
}