import TabsView from '@/layouts/tabs'
import BlankView from '@/layouts/blankView'

const routes = [
  {
    path: process.env.VUE_APP_HOME_PATH,
    name: '首页',
    component: TabsView,
    redirect: '/dashboard/workplace',
    children:[
      {
        path: 'dashboard',
        name: '仪表盘',
        meta: {
          icon: 'AppstoreOutlined',
        },
        component: BlankView,
        children:[
          {
            path:'workplace',
            name:'工作台',
            component: () => import('@/views/dashboard/workplace')
          },
          {
            path:'analysis',
            name:'分析页',
            component: () => import('@/views/dashboard/analysisView')
          },
        ]
      },
      // {
      //   path: 'form',
      //   name: '表单页',
      //   meta: {
      //     icon: 'FormOutlined',
      //   },
      //   component: BlankView,
      //   children:[
      //     {
      //       path: 'basic',
      //       name:'基础表单',
      //       component: () => import('@/views/form/basicForm')
      //     },
      //   ]
      // },
      {
        path: 'list',
        name: '列表页',
        meta: {
          icon: 'TableOutlined',
        },
        children: [
          {
            path: 'table',
            name: '查询表格',
            component: () => import('@/views/list/table/table')
          },
          {
            path:'list',
            name:'基础列表',
            component: () => import('@/views/list/baseList/baseList')
          },
          // {
          //   path:'search',
          //   name:'搜索列表',
          //   component: BlankView,
          //   children: [
          //     {
          //       path:'article',
          //       name:'文章',
          //       component: () => import('@/views/list/search/articleList')
          //     },
          //   ]
          // },
        ]
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: () => import('@/views/exception/404')
  },
  {
    path: '/login',
    name: '登录页',
    component: () => import('@/views/login/login')
  }
]

export default routes