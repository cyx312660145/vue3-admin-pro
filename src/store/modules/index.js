import menu from './menu'
import tabs from './tabs'
import setting from './setting'
import user from './user'

export default {menu, tabs, setting, user}