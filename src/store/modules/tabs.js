import { removeTabs } from "@/utils/routerUtil"
import { get_localStorage, set_localStorage } from "@/utils/util"

// 用于存储 tabs信息

export default{
  namespaced: true,
  state(){
    return{
      tabsList:[]
    }
  },
  getters:{
    tabsList(state){
      if(state.tabsList.length <= 1){
        const tabsList = JSON.parse(get_localStorage('tabsList'))
        if(tabsList){
          state.tabsList = tabsList
        }
      }
      return state.tabsList
    }
  },
  mutations:{
    addTabs(state, tabItem){
      if(tabItem.fullPath == process.env.VUE_APP_HOME_FULL_PATH){
        state.tabsList.unshift(tabItem)
      }else{
        state.tabsList.push(tabItem)
      }
      set_localStorage('tabsList', JSON.stringify(state.tabsList))
    },
    setTabs(state, tabsList){
      state.tabsList = [...tabsList] // 防止无法刷新的bug
      set_localStorage('tabsList', JSON.stringify(tabsList))
    },
    removeTabs(state, tabKey){
      removeTabs(tabKey)
    },
  }
}