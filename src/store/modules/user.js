export default {
  namespaced: true,
  state:{
    userInfo: null
  },
  getters:{
    isLogin(state){
      const userInfo = sessionStorage.getItem('userInfo')
      state.userInfo = state.userInfo || (userInfo ? JSON.parse(userInfo) : null)
      if(state.userInfo !== null){
        return true
      }else{
        return false
      }
    },
    getUserInfo(state){
      return state.userInfo
    }
  },
  mutations:{
    login(state, userInfo){
      state.userInfo = userInfo
      sessionStorage.setItem('userInfo', JSON.stringify(userInfo))
    },
    logout(state){
      state.userInfo = null
      sessionStorage.removeItem('userInfo')
    }
  }
}