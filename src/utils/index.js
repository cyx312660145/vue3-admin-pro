import {setAppOptions, loadRoutes, loadGuards} from './routerUtil'

/**
 *工具初始化
 *
 * @param {*} router 路由
 * @param {*} store Vuex
 */
function initUtil(router, store){
  // 初始化 routerUtil 
  setAppOptions(router, store)
  // 加载路由
  loadRoutes()
  // 加载路由守卫
  loadGuards()
}

export default initUtil