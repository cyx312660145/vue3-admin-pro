import { toRaw } from "vue"
import guards from "./guards" 
import ignore from "@/config/ignore"


const appOptions = {
  router: undefined,
  store: undefined,
}

/**
 *设置全局router,Vuex信息
 *
 * @param {*} router
 * @param {*} store
 */
function setAppOptions(router, store){
  appOptions.router = router
  appOptions.store = store
}

/**
 * 为路由添加一些参数，类似 fullPath、openKeys
 * 
 * @param {*} routes 路由参数
 */
function routesSetting(routes){
  let routesAddOptions = (routes, parentPath=process.env.VUE_APP_HOME_PATH, openKeys=[]) =>{
    routes.forEach(muenItem => {
      muenItem.fullPath = parentPath != '/' ? parentPath + '/' + muenItem.path : parentPath + muenItem.path
      if(openKeys.length == 0){
        muenItem.openKeys = [parentPath, muenItem.fullPath]
      }else{
        muenItem.openKeys = openKeys.concat([muenItem.fullPath])
      }
      const children = muenItem.children
      children && routesAddOptions(children, muenItem.fullPath, muenItem.openKeys)
    })
  }
  routesAddOptions(routes)
  return routes
}

/**
 *加载路由
 *筛选，1.path为 '首页'下的路由为 menu 路由
 */
function loadRoutes(){
  const {router, store} = appOptions
  const homeRoutes = router.options.routes.find(e=>e.path === process.env.VUE_APP_HOME_PATH)
  const menuRoutes = homeRoutes && homeRoutes.children
  if(menuRoutes){
    routesSetting(menuRoutes)
    store.commit("menu/setMenuData", menuRoutes)
    // 添加默认主页tabs
    store.dispatch('menu/getMenuItem', {
      fullPath: process.env.VUE_APP_HOME_FULL_PATH
    }).then((route) => {
      addRouterTabs(route.fullPath, route.name, false)
    });
  }
}


/**
 * 加载路由守卫
 *
 */
function loadGuards(){
  const {router, store} = appOptions
  const {beforeEach} = guards
  beforeEach.forEach(guard=>{
    if(guard && typeof guard === 'function'){
      router.beforeEach((to, from, next)=>{
        return guard(store, to, from, next)
      })
    }
  })
}


/**
 * 添加tabs导航
 *
 * @param {*} fullPath
 * @param {*} name
 */
function addRouterTabs(fullPath, name){
  const {store} = appOptions
  const tabsList = store.getters['tabs/tabsList']
  const item = tabsList.find(e => e.fullPath == fullPath)
  store.dispatch('menu/getMenuItem', {
    fullPath: fullPath
  }).then((result) => {
    // 添加tabs, 先判断tabs是否存在，且不与ignore匹配
    // result用于判断是否为404页面
    if (!item && !ignore.includes(fullPath) && result) {
      const closable = fullPath == process.env.VUE_APP_HOME_FULL_PATH ? false : true
      store.commit('tabs/addTabs', {
        fullPath,
        key: fullPath,
        name,
        closable,
      })
    }
  });
}

/**
 * 路由跳转
 *
 * @param {*} fullPath
 * @param {string} [query=""]
 */
function routerTo(fullPath, query=""){
  const {router} = appOptions
  router.push({path: fullPath, query})
}

/**
 * 删除tabs
 *
 * @param {*} key
 */
function removeTabs(tabKey){
  const {store} = appOptions
  const selectedKeys = toRaw(store.getters['menu/selectedKeys'])[0]
  const tabsList = toRaw(store.getters['tabs/tabsList'])
  let idx = tabsList.findIndex(e=>e.fullPath == tabKey)
  if(selectedKeys == tabKey){
    let res
    if(tabsList[idx + 1] != undefined){
      res = tabsList[idx + 1]
    }else{
      res = tabsList[idx - 1]
    }
    routerTo(res.fullPath)
  }
  tabsList.splice(idx,1)
  store.commit('tabs/setTabs', tabsList)
}

export {setAppOptions, loadRoutes, loadGuards, addRouterTabs, routerTo, removeTabs}