import enquireJs from 'enquire.js'

const _toString = Object.prototype.toString

export function isDef (v){
  return v !== undefined && v !== null
}

/**
 * Remove an item from an array.
 */
export function remove (arr, item) {
  if (arr.length) {
    const index = arr.indexOf(item)
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

export function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

export function enquireScreen(call) {
  const handler = {
    match: function () {
      call && call(true)
    },
    unmatch: function () {
      call && call(false)
    }
  }
  enquireJs.register('only screen and (max-width: 767.99px)', handler)
}

/**
 * 清除缓存方法
 *
 * @export
 */
export function clear_all() {
  const key_list = ['tabsList']
  key_list.forEach(key => {
    clear_localStorage(key)
  })
}

/**
 * 设置localStorage
 * key 要加上 VUE_APP_STORAGE_NAME
 * @export
 * @param {*} key
 * @param {*} data
 */
export function set_localStorage(key, data) {
  const _key = process.env.VUE_APP_STORAGE_NAME + '_' + key
  localStorage.setItem(_key, data)
}

/**
 * 获取localStorage
 *
 * @export
 * @param {*} key
 * @returns
 */
export function get_localStorage(key) {
  const _key = process.env.VUE_APP_STORAGE_NAME + '_' + key
  return localStorage.getItem(_key)
}

/**
 * 清除 localStorage
 *
 * @export
 * @param {*} key
 */
export function clear_localStorage(key) {
  const _key = process.env.VUE_APP_STORAGE_NAME + '_' + key
  localStorage.removeItem(_key)
}