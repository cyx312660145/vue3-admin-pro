const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: '/' + process.env.VUE_APP_KEY + '/', //gitee远程库名
  outputDir: 'dist/',
  assetsDir: 'static/',
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    }
  },
})
